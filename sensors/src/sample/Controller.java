package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import sensors.Sensor;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public TableView<Sensor> table;
    public TableColumn<Sensor, String> id;
    public TableColumn<Sensor, String> min;
    public TableColumn<Sensor, String> max;
    public TableColumn<Sensor, String> period;
    public TableColumn<Sensor, Button> start;
    public TableColumn<Sensor, Button> stop;
    public TableColumn<Sensor, Button> delete;
    public TextField host;
    public TextField port;
    private int counter = 0;

    public static ObservableList<Sensor> data = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new ConnectionData(host.getText(), Integer.parseInt(port.getText()));
        initTable();
        loadData();
        for (Sensor item : table.getItems()) {
             System.out.println(item.sendIDandValue());
        }
    }

    private void initTable() {
        initCols();
    }

    private void initCols() {
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        min.setCellValueFactory(new PropertyValueFactory<>("min"));
        max.setCellValueFactory(new PropertyValueFactory<>("max"));
        period.setCellValueFactory(new PropertyValueFactory<>("period"));
        start.setCellValueFactory(new PropertyValueFactory<>("start"));
        stop.setCellValueFactory(new PropertyValueFactory<>("stop"));
        delete.setCellValueFactory(new PropertyValueFactory<>("delete"));

        editCols();
    }

    private void editCols() {
        id.setCellFactory(TextFieldTableCell.forTableColumn());
        id.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setId(Integer.parseInt(event.getNewValue()));
        });

        min.setCellFactory(TextFieldTableCell.forTableColumn());
        min.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setMin(Double.parseDouble(event.getNewValue()));
        });

        max.setCellFactory(TextFieldTableCell.forTableColumn());
        max.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setMax(Double.parseDouble(event.getNewValue()));
        });

        period.setCellFactory(TextFieldTableCell.forTableColumn());
        period.setOnEditCommit(event -> {
            event.getTableView().getItems().get(event.getTablePosition().getRow()).setPeriod(Integer.parseInt(event.getNewValue()));
        });

        table.setEditable(true);

    }

    private void loadData() {
        for (int i = 0; i < 1; i++) {
            data.add(new Sensor(i, 0, 100, 1000,
                    new Button("Start"), new Button("Stop"), new Button("Delete")));
            counter++;
        }
        table.setItems(data);
    }

    public void addPress(ActionEvent actionEvent) {
        data.add(new Sensor(counter, 0, 100, 1000,
                new Button("Start"), new Button("Stop"), new Button("Delete")));
        counter++;
    }
}