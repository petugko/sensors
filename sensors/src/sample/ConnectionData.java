package sample;

public class ConnectionData {
    public static int port = 9000;
    public static String domain = "localhost";

    public ConnectionData(String domain, int port) {
        this.port = port;
        this.domain = domain;
    }
}
