package sensors;

import javafx.scene.control.Button;
import sample.ConnectionData;
import sample.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Sensor implements Runnable{
    private Socket client;
    private PrintWriter output;

    private Random random;
    private double min, max, previous;

    private int id, period;
    private DateFormat dateFormat;
    private Date date;

    private Button start, stop, delete;

    private boolean isPaused;
    private boolean isRunning;


    public Sensor(int id, double min, double max, int period, Button start, Button stop, Button delete) {

        this.id = id;
        this.min = min;
        this.max = max;
        this.period = period;
        this.start = start;
        this.stop= stop;
        this.delete = delete;

        this.random = new Random();
        this.previous = Double.MAX_VALUE;

        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();

        isPaused = false;
        isRunning= true;
        buttons();

        try {
            client = new Socket(ConnectionData.domain, ConnectionData.port);
            //client = new Socket("localhost", 9000);
            output = new PrintWriter(client.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public double getNextValue() {
        double rand = random.nextDouble() * (max - min) + min;
        double deversity = previous * 0.05;
        while (true) {
            if (previous == Double.MAX_VALUE) {
                rand = random.nextDouble() * (max - min) + min;
                previous = rand;
                break;
            }

            if ((previous - deversity) <= rand && (previous + deversity) >= rand) {
                previous = rand;
                break;
            } else {
                rand = random.nextDouble() * (max - min) + min;
            }
        }
        return rand;
    }

    public String sendIDandValue() {
        return  "" + id  + ";" + getNextValue() + ";" + dateFormat.format(date);
    }

    @Override
    public void run() {
        String sendedData = "";
        while (isRunning) {
            if (!isPaused) {
                sendedData = sendIDandValue();
                //System.out.println(x);
                output.println(sendedData);
                output.flush();

                try {
                    Thread.sleep(period);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void buttons() {
        start.setOnAction(event -> {
            for (Sensor sen : Controller.data) {
                if (sen.getStart() == start) {
                    isPaused = false;
                    isRunning = true;
                    Thread thread = new Thread(this);
                    thread.start();
                }
            }
        });

        stop.setOnAction(event -> {
            for (Sensor sen : Controller.data) {
                if (sen.getStop() == stop) {
                    isRunning = false;
                }
            }
        });

        delete.setOnAction(event -> {
            Sensor sensorHelp = null;
            for (Sensor sen : Controller.data) {
                if (sen.getDelete() == delete) {
                    sensorHelp = sen;
                }
            }
            Controller.data.remove(sensorHelp);
        });
    }



    public void setMin(double min) {
        this.min = min;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId() {
        return String.valueOf(id);
    }

    public String getMin() {
        return String.valueOf(min);
    }

    public String getMax() {
        return String.valueOf(max);
    }

    public Button getStart() {
        return start;
    }

    public void setStart(Button start) {
        this.start = start;
    }

    public Button getStop() {
        return stop;
    }

    public void setStop(Button stop) {
        this.stop = stop;
    }

    public Button getDelete() {
        return delete;
    }

    public void setDelete(Button delete) {
        this.delete = delete;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public String getPeriod() {
        return String.valueOf(period);
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
