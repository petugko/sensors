import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import io.reactivex.Single;

import javax.json.Json;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class PortListener implements Runnable {

    private InputStreamReader in;
    private BufferedReader buffReader;
    private HubConnection hubConnection;

    public PortListener(Socket server, HubConnection hubConnection) throws IOException {
        in = new InputStreamReader(server.getInputStream());
        buffReader = new BufferedReader(in);
        this.hubConnection = hubConnection;
    }

    public void run() {
        System.out.println("client connected");

        String str = "";

        System.out.println(hubConnection.getConnectionState().toString());
        while (true) {
            try {
                //input
                str = buffReader.readLine();

                //output
                if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED) {
                    ParsedObject parsedObject = parseOutput(str);
                    hubConnection.send("SensorDataReciever", parsedObject.getId(), parsedObject.getValue(), parsedObject.getDate());
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                break;
            }
            if (str == null)
                break;
            System.out.println("client: " + str);
        }
    }

    private static String getMessage(String message) {
        String[] tokens = message.split(";");

        return Json.createObjectBuilder()
                .add("SensorNumber", tokens[0])
                .add("Value", tokens[1])
                .add("CreatedAt",tokens[2])
                .build()
                .toString();
    }

    private ParsedObject parseOutput(String parsedText) {
        String[] components = parsedText.split(";");
        return new ParsedObject(components[0], Double.parseDouble(components[1]), components[2]);
    }
}
