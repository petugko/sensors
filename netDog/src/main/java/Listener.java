import com.microsoft.signalr.HubConnection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Listener {

    private HubConnection hubConnection;
    private ServerSocket serverSocket;

    public Listener() throws IOException {
        serverSocket = new ServerSocket(ConnectionData.portS);
    }

    public Listener(HubConnection hubConnection) throws IOException {
        serverSocket = new ServerSocket(ConnectionData.portS);
        this.hubConnection = hubConnection;
    }

    public void run()  throws IOException {
        int i = 1;
        while (true) {
            Socket server = serverSocket.accept();
            Thread thread = new Thread(new PortListener(server, hubConnection), "Client " + i);
            thread.start();
            i++;
        }
    }
}
