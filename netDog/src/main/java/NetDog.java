import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import io.reactivex.Single;
import sun.util.calendar.LocalGregorianCalendar;

import java.io.IOException;
import java.util.Date;

public class NetDog {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("NetDog starting...");
        new ConnectionData("wss://echo.websocket.org", 9001,"localhost", 9000);

        HubConnection hubConnection = HubConnectionBuilder.create("ws://localhost:50029/ws-sensor")
                .withAccessTokenProvider(Single.defer(() -> {
                    // Your logic here.
                    return Single.just("eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJFbXBsb3llZSI6IntcIklkXCI6MixcIkJpcnRoTnVtYmVyXCI6XCJzdHJpbmdcIixcIkxvZ2luXCI6XCJnYXRlXCIsXCJQYXNzd29yZFwiOlwiJDJhJDEwJC9WVTA0cVlLaDVIbDVqOGRVU3hqUE9sOFZjejFRYU9mRklNR25nTTZ3QWI4TElDZy5mZUZxXCIsXCJFbWFpbFwiOlwic3RyaW5nXCIsXCJQb3NpdGlvblwiOlwic3RyaW5nXCIsXCJFbXBsb3llZEZyb21cIjpcIjIwMTktMTAtMDhUMTc6MDU6MjhcIixcIkVtcGxveWVkVG9cIjpcIjIwMTktMTAtMDhUMTc6MDU6MjhcIixcIk1vZHVsZXNcIjpudWxsLFwiVXNlclR5cGVcIjpudWxsLFwiQWN0aXZlXCI6MCxcIlNoYXJpbmdcIjpudWxsLFwiRW1wQXBpS2V5XCI6bnVsbH0iLCJleHAiOjE5MzE2NDQyNTksImlzcyI6InNtZXNrLmluIiwiYXVkIjoicmVhZGVycyJ9.LlfZYgoVZ69oVOulpeZQIRm6LYskiqEsX-ueyGLxprw");
                })).build();

        hubConnection.start();

        Listener listener = new Listener(hubConnection);
        listener.run();
    }
}