public class ParsedObject {

    private String id;
    private double value;
    private String date;

    ParsedObject(String id, double vaule, String date) {
        this.date = date;
        this.value = vaule;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
