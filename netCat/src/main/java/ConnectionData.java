
public class ConnectionData {
    public static String hostC = "wss://echo.websocket.org";
    public static int portC = 9001;
    public static String hostS = "localhost";
    public static int portS = 9000;
//ws://localhost:50029/ws-sensor

    public ConnectionData(String hostC, int portC, String hostS, int portS) {
        this.hostC = hostC;
        this.portC = portC;
        this.hostS = hostS;
        this.portS = portS;
    }
}
