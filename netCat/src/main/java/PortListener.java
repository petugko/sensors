import javax.json.Json;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;

public class PortListener implements Runnable {

    private InputStreamReader in;
    private BufferedReader buffReader;
    private WebSocketClient clientEndPoint;

    public PortListener(Socket server) throws IOException {
        in = new InputStreamReader(server.getInputStream());
        buffReader = new BufferedReader(in);

        try {
            clientEndPoint = new WebSocketClient(new URI("wss://echo.websocket.org"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        System.out.println("client connected");

        String str = "";
        while (true) {
            try {
                //input
                str = buffReader.readLine();

                //output
                clientEndPoint.addMessageHandler(new WebSocketClient.MessageHandler() {
                    public void handleMessage(String message) {
                        System.out.println(message);
                    }
                });
                clientEndPoint.sendMessage(getMessage(str));

            } catch (IOException e) {
                break;
            }
            if (str == null)
                break;
            System.out.println("client: " + str);
        }
    }

    private static String getMessage(String message) {
        String[] tokens = message.split(";");

        return Json.createObjectBuilder()
                .add("SensorNumber", tokens[0])
                .add("Value", tokens[1])
                .add("CreatedAt",tokens[2])
                .build()
                .toString();
    }
}
