import java.io.IOException;

public class NetCat {

    public static void main(String[] args) throws IOException {
        new ConnectionData("wss://echo.websocket.org", 9001,"localhost", 9000);
        Listener listener = new Listener();
        listener.run();
    }
}