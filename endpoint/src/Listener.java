import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Listener {

    private ServerSocket ss;

    public Listener() throws IOException {
        ss = new ServerSocket(9000);
    }

    public void run() throws IOException {
        int i = 1;
        while (true) {
            Socket s = ss.accept();
            Thread thread = new Thread(new PortListener(s), "Client " + i);
            thread.start();
            i++;
        }
    }
}
