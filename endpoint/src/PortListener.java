import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class PortListener implements Runnable {

    private InputStreamReader in;
    private BufferedReader bf;
    private Socket s;

    public PortListener(Socket s) throws IOException {
        in = new InputStreamReader(s.getInputStream());
        bf = new BufferedReader(in);
    }

    @Override
    public void run() {
        System.out.println("client connected");

        String str = "";
        while (true) {
            try {
                str = bf.readLine();
            } catch (IOException e) {
                break;
            }
            if (str == null)
                break;
            System.out.println("client: " + str);
        }
    }
}
